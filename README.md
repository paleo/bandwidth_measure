# Bandwidth measure

## About

This repository contains programs for measuring the data transfer performances of various tools (memcached, redis, MPI, ...) with the same measurement methods.
For now, only the `memcached` part is done.


## Usage

```
pip install .
memcached-stress-write [--help]
memcached-stress-read [--help]
memcached-stress-readwrite [--help]
```

## Warning: work in progress

This is a work in progress. This means that the project structure can change at any time.