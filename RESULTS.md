# Memcached performances


## Method

Server configuration:

```bash
memcached --listen=0.0.0.0 --memory-limit=64000 --max-item-size=16m --slab-min-size=16m
```

Measurement protocol :

  - The writer sends *Nw* frames to the server. By default *Nw = 100*, and the frames are 2048*2048 uint16 frames.
  - The reader reads *Nr* frames as fast as possible. By default *Nr = 100*.

## Results


| Server name / interface | Write     | Read      | Write + Read            |
|-------------------------|-----------|-----------|-------------------------|
| scisoft13 / local       | 1300 MB/s | 1550 MB/s | 1300 MB/s  /  1500 MB/s |
| scisoft13 <-> scisoft14 |           |           |                         |
| power9 / local          | 650 MB/s  | 1200 MB/s | 2400 MB/s  / 1030 MB/s  |
| power9 / taskset        | 2400 MB/s | 1230 MB/s | 2400 MB/s / 1300 MB/s   |


## Notes

We can draw the following conclusions so far :

  - Performances vary notably when constraining the CPU affinity (with `taskset`).
  - Simultaneous read and write does not seem to affect performances, at least with 1 writer / 1 reader.
  - Surprisingly, write seems to be faster than read on power9.


# ZeroMQ performances

On power9, memcached write operations appear to be faster than read operations.
In order to see if it comes from a limitation of memcached, or the machine itself, we measure the data bandwidth using zeromq.

## Method

We use a ZeroMQ "push-pull" communication pattern. 
By default, the push client tracks the sent messages to ensure that they were sent (but it does not track the receive status). 

On the push side :
```bash
zmq-stress-push --push_nocopy 1 --push_track 0
```
On the pull side:
```bash
zmq-stress-pull
```

## Results


| Server name / interface | Write     | Read      |
|-------------------------|-----------|-----------|
| scisoft13 / local       | todo      | todo      |
| scisoft13 <-> scisoft14 | todo      | todo      |
| power9 / local          | 9000 MB/s | 4500 MB/s |

The figures are better than with memcached, but using sockets comes with many drawbacks when it comes to ODA, as the useful features have to be implemented from scratch (data remanence, multi-clients, etc).

Here again, the write operation is faster than the read operation, even when setting `--push_nocopy 0`.  
The performances are less sensitive to CPU affinity when using ZMQ, certainly due to the one-one read-write nature of the communication pattern.

Therefore, it seems that the reads being twice slower than writes is not a limitation of memcached.
