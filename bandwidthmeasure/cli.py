#!/usr/bin/env python
# coding: utf-8

from multiprocessing import Process
from .client import MemcachedClient
from .utils import parse_params_values
from .zeromq import ZMQPush, ZMQPull
from .config import Client_config, ZMQ_config


def test_write():
    print("Running write benchmark")
    params = parse_params_values(Client_config)

    C = MemcachedClient(params)
    if params["stream_length"] <= 0:
        MB_s = C.send_data()
    else:
        MB_s = C.send_data_stream()

    N = max(params["write_requests"], params["stream_length"])
    print("Write MB/s (%d requests) : %.2f" % (N, MB_s))


def test_read():
    print("Running read benchmark")
    params = parse_params_values(Client_config)

    C = MemcachedClient(params)
    if len(params["frames_subset"]) == 0:
        MB_s, n_fails = C.receive_data()
        n_frames = C.params["read_requests"]
    else:
        MB_s, n_fails, n_suc = C.recv_frames_subset()
        n_frames = n_suc + n_fails
    print(
        "Read MB/s (%s requests, %d fails) : %.2f" %
        (n_frames, n_fails, MB_s)
    )


def test_read_write():
    print("Running simultaneous write/read benchmark")

    # Writer will write "N_w" items, reader will read "N_r" items.
    # We first write N_r values to ensure that the reader will grab values.
    params = parse_params_values(Client_config)
    C = MemcachedClient(params)
    C.send_data(num_writes=params["read_requests"])

    # Then, we run reader and writer simultaneously.
    writer_process = Process(
        target=test_write,
    )
    reader_process = Process(
        target=test_read,
    )
    writer_process.start()
    reader_process.start()




def test_zmq_push():
    params = parse_params_values(ZMQ_config)
    Push = ZMQPush(params)
    MB_s = Push.push()
    print("Push MB/s (%d requests) : %.2f" % (params["num_requests"], MB_s))

def test_zmq_pull():
    params = parse_params_values(ZMQ_config)
    Pull = ZMQPull(params)
    MB_s = Pull.pull()
    print("Pull MB/s (%d requests) : %.2f" % (params["num_requests"], MB_s))



def test_zmq():
    print("Testing ZMQ IPC socket performance")

    pull_process = Process(
        target=test_zmq_pull,
    )
    push_process = Process(
        target=test_zmq_push,
    )
    pull_process.start()
    push_process.start()

