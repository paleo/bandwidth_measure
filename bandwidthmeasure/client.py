import numpy as np
from time import time
from pymemcache import Client
from .utils import parse_params_values, default_config
from .config import Client_config

class MemcachedClient(object):

    def __init__(self, client_params):
        """
        Instantiate a memcached client.

        Parameters
        ----------
        client_params: dict
            A dictionary of the client configuration.
        """

        self.apply_config(client_params)
        self.connect()

    def apply_config(self, client_params):
        client_params = client_params or {}
        config = default_config(Client_config)
        config.update(client_params)
        self.params = config
        rs = self.params["request_size"]
        if isinstance(rs, str):
            self.frame_shape = tuple(map(int, rs.replace("(", "").replace(")", "").split(",")))
        else:
            self.frame_shape = rs
        self.frame_dtype = np.uint16 # TODO custom ?
        self.frame_bytes = np.iinfo(self.frame_dtype).bits //8  * np.prod(self.frame_shape)


    def connect(self):
        self.client = Client((self.params["host"], self.params["port"]))
        self._precomputed = False


    #
    # Test case 1
    #


    def precompute_data(self, n=None):
        if self._precomputed and n is None:
            return
        print("Pre-computing frames...")
        shape = self.frame_shape
        dtype = self.frame_dtype
        frames = []
        n_writes = n or self.params["write_requests"]
        for i in range(n_writes):
            frame = np.random.randint(
                0,
                high=np.iinfo(dtype).max+1,
                size=shape,
                dtype=dtype
            ).tobytes()
            frames.append(frame)
        self.frames = frames
        self._precomputed = True
        print("... frames precomputed")


    def send_data(self, num_writes=None):
        self.precompute_data(n=num_writes)
        t0 = time()
        i0 = self.params["start_num"]
        i = i0
        for frame in self.frames:
            frame_name = self.params["key_prefix"] + str("%04d" % i)
            self.client.set(frame_name, frame, noreply=self.params["noreply"])
            i += 1
        t1 = time()
        print(t1 - t0) # DEBUG
        MB_s = self.frame_bytes * (i - i0) / 1e6 /(t1 - t0)
        return MB_s


    def receive_data(self, num_reads=None):
        n = num_reads or self.params["read_requests"]
        _ = self.client.get(self.params["key_prefix"] + "0000") # warmup - DEBUG
        t0 = time()
        i0 = self.params["start_num"]
        print_missed = self.params["print_missed"]
        n_suc = 0
        n_fails = 0
        for i in range(i0, i0+n):
            frame_name = self.params["key_prefix"] + str("%04d" % i)
            rep = self.client.get(frame_name)
            if rep is None:
                n_fails += 1
                if print_missed:
                    print("Receiving frame %s failed" % frame_name)
            else:
                n_suc += 1
        t1 = time()
        print(t1 - t0) # DEBUG
        MB_s = self.frame_bytes * n_suc / 1e6 /(t1 - t0)
        return MB_s, n_fails


    #
    # Test case 2
    #

    def send_data_stream(self):
        self.precompute_data()
        stream_length = self.params["stream_length"]
        n_prec_frames = self.params["write_requests"]
        name_prefix = self.params["key_prefix"]
        noreply = self.params["noreply"]
        t0 = time()
        i0 = self.params["start_num"]
        for i in range(i0, i0 + stream_length):
            frame_name = name_prefix + str("%04d" % i)
            self.client.set(frame_name, self.frames[i % n_prec_frames], noreply=noreply)
        t1 = time()
        print(t1 - t0) # DEBUG
        MB_s = self.frame_bytes * (i - i0)/ 1e6 /(t1 - t0)
        return MB_s


    @staticmethod
    def frames_subset(subset):
        a, b = subset.split(",")
        return int(a.strip()), int(b.strip())

    def recv_frames_subset(self):
        name_prefix = self.params["key_prefix"]
        # get frame with numbers k*S+r
        S, r = self.frames_subset(self.params["frames_subset"])
        print("Asking frames numbers %d*k+%d" % (S, r))
        n = self.params["read_requests"]
        print_missed = self.params["print_missed"]
        n_fails, n_suc = 0, 0
        t0 = time()
        i0 = self.params["start_num"]
        for i in range(i0, i0+n):
            if i % S != r:
                continue
            frame_name = name_prefix + str("%04d" % i)
            rep = self.client.get(frame_name)
            if rep is None:
                n_fails += 1
                if print_missed:
                    print("Receiving frame %s failed" % frame_name)
            else:
                n_suc += 1
        t1 = time()
        MB_s = self.frame_bytes * (n_suc) / 1e6 /(t1 - t0)
        return MB_s, n_fails, n_suc



