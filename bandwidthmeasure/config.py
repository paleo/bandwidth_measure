#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Not used
Server_config = {
    "listen": {
        "help": "Listening host",
        "default": "0.0.0.0",
    },
    "port": {
        "help": "Listening port",
        "default": 11211,
        "type": int,
    },
    "memory_limit": {
        "help": "Memory limit in GB",
        "default": 1,
        "type": int,
    },
    "max_item_size": {
        "help": "Max size of each slab.",
        "default": "16m",
    },
    "slab_min_size": {
        "help": "Minimum size for key-values-flags",
        "default": "16m",
    },
    "threads": {
        "help": "Number of threads to use to handle requests",
        "default": 4,
        "type": int,
    },
}

Client_config = {
    "host": {
        "help": "Memcached server listening address",
        "default": "127.0.0.1",
    },
    "port": {
        "help": "Memcached server listening port",
        "default": 11211,
        "type": int,
    },
    "use_reader": {
        "help": "Spawn a reader client in addition to the writer client. This is for doing the simultaneous read/write tests.",
        "default": 0,
        "type": int,
    },
    "write_requests": {
        "help": "Total number of write requests",
        "default": 100,
        "type": int,
    },
    "read_requests": {
        "help": "Total number of read requests, when using a reader",
        "default": 100,
        "type": int,
    },
    "request_size": {
        "help": "Shape of each write request",
        "default": (2048, 2048),
    },
    "key_prefix": {
        "help": "Prefix of each key",
        "default": "data_",
    },
    "noreply": {
        "help": "Whether the client must wait the memcached server acknowledgement when sending data",
        "default": True,
        "type": bool,
    },
    "stream_length": {
        "help": "If set, this will run the second test case. A stream of frames is sent, but only 'write_requests' frames are precomputed. After that, the writer loops over the precomputed frames.",
        "default": 0,
        "type": int,
    },
    "frames_subset": {
        "help": "Indicate which subset of frames the reader should ask for. Leave empty for all frames. '2-0' indicates even frame numbers, '2,1' indicates odd frame numbers, '3,0' indicates frames number multiple of 3, '3,1' indicates frames number 3*k+1, and so on.",
        "default": "",
    },
    "print_missed": {
        "help": "Indicate whether the missed items should be printed.",
        "default": False,
        "type": bool,
    },
    "start_num": {
        "help": "Starting number of frame. Useful for making read/write requests one after the other.",
        "default": 0,
        "type": int,
    },
}


ZMQ_config = {
    "num_requests": {
        "help": "Total number of requests, when using a reader",
        "default": 100,
        "type": int,
    },
    "request_size": {
        "help": "Shape of each write request",
        "default": (2048, 2048),
    },
    "push_wait_time": {
        "help": "Waiting time (in s) before terminating the push process. Set to a negative value for waiting indefinitely.",
        "default": 1.,
        "type": float,
    },
    "push_nocopy": {
        "help": "Advanced ZMQ parameter. Whether to use zerocopy when sending data.",
        "default": False,
        "type": bool,
    },
    "push_track": {
        "help": "Advanced ZMQ parameter. Whether to track for message delivery on the sender side. Ignored if push_nocopy is False.",
        "default": False,
        "type": bool,
    },


}





