#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .utils import exec_subprocess, get_ram_infos, parse_params_values
from .config import Server_config


class MemcachedServer(object):

    def __init__(self, server_params):
        """
        Spawn a memcached server.

        Parameters
        -----------
        server_params: dict
            A dictionary of memcached parameters.
        """
        self.parse_params(server_params)
        self.foolproof()


    def parse_params(self, params):
        self.params = params
        print(params)
        self.memcached_cmd = "memcached "
        for k, v in params.items():
            if k == "memory_limit":
                v = int(v * 1e9)
            self.memcached_cmd += str("%s=%s " % (k.replace("_", "-"), v))

    def foolproof(self):
        ram_infos = get_ram_infos()
        avail_ram = ram_infos["total_GB"] * (1 - ram_infos["used_pc"]/100.)
        if self.params["memory_limit"] > avail_ram / 2.:
            print("Warning: You requested more than 50%% than remaining RAM.")
            if input("Please confirm that you want to continue [y/N]") != "y":
                raise RuntimeError("Too much RAM requested")

    def start(self):
        self.memcached_process = exec_subprocess(
            self.memcached_cmd,
            use_shell=False
        )



if __name__ == "__main__":
    params = parse_params_values(Server_config)
    M = MemcachedServer(params)
    M.start()
