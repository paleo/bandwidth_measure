import subprocess
from argparse import ArgumentParser
from psutil import virtual_memory

def parse_params_values(Params, parser_description=None):
    """
    Usage:

       ```python
       from param_file import Params
       params = parse_params_values(Params)
       ```
    """

    parser = ArgumentParser(description=parser_description)

    for param_name, vals in Params.items():
        parser.add_argument("--" + param_name, **vals)

    args = parser.parse_args()
    args_dict = args.__dict__
    return args_dict


def default_config(Params):
    conf = {}
    for k, v in Params.items():
        conf[k] = Params[k]["default"]
    return conf


def exec_subprocess(cmd, use_shell=False):
    if not(use_shell):
        cmd = cmd.split()
    p = subprocess.Popen(
        cmd,
        shell=use_shell,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    out, err = p.communicate()
    return err, out


def get_ram_infos():
    v = virtual_memory()
    ram_infos = {
        "total_GB": v.available / 1e9,
        "used_GB": v.used / 1e9,
        "used_pc": v.percent,
    }
    return ram_infos

