#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import zmq
from time import time, sleep
from .utils import parse_params_values, default_config
from .config import ZMQ_config

class ZMQTest(object):

    def __init__(self, zmq_params):
        self.apply_config(zmq_params)

    def apply_config(self, params):
        params = params or {}
        config = default_config(ZMQ_config)
        config.update(params)
        self.params = config
        self.frame_shape = self.params["request_size"]
        self.frame_dtype = np.uint16 # TODO custom ?
        self.frame_bytes = np.iinfo(self.frame_dtype).bits //8  * np.prod(self.frame_shape)
        self._i = 0



class ZMQPull(ZMQTest):
    def __init__(self, zmq_params):
        super().__init__(zmq_params)
        self._connect()

    def _connect(self):
        context = zmq.Context()
        socket = context.socket(zmq.PULL)
        socket.setsockopt(zmq.LINGER, 0)
        socket.bind("ipc:///tmp/zmq0001") # TODO custom ?
        self.ctx = context
        self.socket = socket

    def pull(self):
        datas = []
        # Trigger the timer at first reception
        datas.append(self.socket.recv())
        t0 = time()
        for i in range(self.params["num_requests"]-1):
            r = self.socket.recv()
            datas.append(r)
            #~ print("[PULL] received %d" % i) # DEBUG
        t1 = time()
        el = (t1 - t0)
        MB_s = self.frame_bytes * (len(datas)-1) / 1e6 /(t1 - t0)
        return MB_s


    receive_data = pull




class ZMQPush(ZMQTest):
    def __init__(self, zmq_params):
        super().__init__(zmq_params)
        self._connect()
        self._precomputed = False

    def _connect(self):
        context = zmq.Context()
        socket = context.socket(zmq.PUSH)
        socket.setsockopt(zmq.LINGER, 0)
        socket.connect("ipc:///tmp/zmq0001")
        self.ctx = context
        self.socket = socket

    def precompute_data(self, force_recompute=False):
        if self._precomputed and not(force_recompute):
            return
        shape = self.frame_shape
        dtype = self.frame_dtype
        frames = []
        for i in range(self.params["num_requests"]):
            frame = np.random.randint(
                0,
                high=np.iinfo(dtype).max+1,
                size=shape,
                dtype=dtype
            ).tobytes()
            frames.append(frame)
        self.frames = frames
        self._precomputed = True

    def push(self):
        use_copy = not(self.params["push_nocopy"])
        track=self.params["push_track"]
        self.precompute_data()
        t0 = time()
        for i, frame in enumerate(self.frames):
            self.socket.send(frame, copy=use_copy, track=track)
            #~ print("[PUSH] sent %d" % i) # DEBUG
        t1 = time()
        el = (t1 - t0)
        MB_s = self.frame_bytes * len(self.frames) / 1e6 /(t1 - t0)

        # The pusher should wait for the puller to recevive all the sent data.
        # If the push process terminates too early, then the LINGERing process
        # will discard all unsent message, making the receiver hang indefinitely.
        # To avoid this, we wait a little bit before closing the pusher.
        wait = self.params["push_wait_time"]
        if wait < 0:
            input("Press any key to stop pusher")
        else:
            sleep(wait)

        return MB_s

    send_data = push
