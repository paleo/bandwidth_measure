#!/usr/bin/env python
# coding: utf-8

from setuptools import setup
import os

def get_version():
    with open("bandwidthmeasure/__init__.py", "r") as fid:
        lines = fid.readlines()
    version = None
    for line in lines:
        if "version" in line:
            version = line.rstrip().split("=")[-1].lstrip()
    if version is None:
        raise RuntimeError("Could not find version from __init__.py")
    version = version.strip("'").strip('"')
    return version


def setup_package():

    version = get_version()

    packages_folders = []
    packages = ["bandwidthmeasure"]
    package_dir = {"bandwidthmeasure": "bandwidthmeasure"}
    for f in packages_folders:
        modulename = str("bandwidthmeasure.%s" % f)
        packages.append(modulename)
        package_dir[modulename] = os.path.join("bandwidthmeasure", f)

    setup(
        name='bandwidthmeasure',
        author='Pierre Paleo',
        version=version,
        author_email = "pierre.paleo@esrf.fr",
        maintainer = "Pierre Paleo",
        maintainer_email = "pierre.paleo@esrf.fr",

        packages=packages,
        package_dir = package_dir,
        package_data = {},
        install_requires = [
            "numpy",
            "pymemcache",
            "psutil",
            "zmq",
        ],
        long_description = """
        bandwidthmeasure
        """,
        entry_points = {
            'console_scripts': [
                "memcached-stress-write=bandwidthmeasure.cli:test_write",
                "memcached-stress-read=bandwidthmeasure.cli:test_read",
                "memcached-stress-readwrite=bandwidthmeasure.cli:test_read_write",
                "zmq-stress-pushpull=bandwidthmeasure.cli:test_zmq",
                "zmq-stress-push=bandwidthmeasure.cli:test_zmq_push",
                "zmq-stress-pull=bandwidthmeasure.cli:test_zmq_pull",
            ],
        },
        zip_safe=True
    )


if __name__ == "__main__":
    setup_package()
